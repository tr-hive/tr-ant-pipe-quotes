 /// <reference path="../typings/tsd.d.ts" />
 import rabbit = require("da-rabbitmq-rx");
 import logs = require("da-logs");
 import Rx = require("rx");
 import * as tn from "da-trader-net-rx"
 
function getEnvVar(name: string) : string {
	return process.env[name] || 
	process.env["npm_config_" + name] || 
	process.env["npm_package_config_" + name];	
}


const TRADER_NET_URL = getEnvVar("TRADER_NET_URL");
const TRADER_NET_API_KEY = getEnvVar("TRADER_NET_API_KEY"); 
const TRADER_NET_SEC_KEY = getEnvVar("TRADER_NET_SEC_KEY");
const RABBIT_URI = getEnvVar("RABBIT_URI");
const RABBIT_QUEUE = getEnvVar("RABBIT_QUEUE_QUOTES");
const TRADER_NET_QUOTES_RECONNECT_TIMEOUT = getEnvVar("TRADER_NET_QUOTES_RECONNECT_TIMEOUT");

var logger = new logs.LoggerCompose({pack : <any>require("../package.json"), tags : ["pipe", "quotes"]}, {console: true}); 
var traderNet;

function reconnect() {
	traderNet.quotesStream
	.map(val => val.filter(f => !!f.latestPrice))
	.filter(val => val.length != 0)
	.subscribe(val => { 
		pub.write(val);
		//logger.write({oprt: "handle", status: "success", quotes : val});
	});
	
	traderNet.startRecieveQuotes(getEnvVar("TRADER_NET_TICKETS").split(","));
}

console.log(TRADER_NET_QUOTES_RECONNECT_TIMEOUT);
Rx.Observable.timer(0, TRADER_NET_QUOTES_RECONNECT_TIMEOUT ? parseInt(TRADER_NET_QUOTES_RECONNECT_TIMEOUT) : undefined)
.flatMap(_ => {
	if (traderNet) {
		traderNet.disconnect();
	}
	traderNet = new tn.TraderNet(TRADER_NET_URL);
	return traderNet.connect({apiKey: TRADER_NET_API_KEY, securityKey: TRADER_NET_SEC_KEY});
})
.do(reconnect)
.subscribe(() =>
	console.log({oper: "tn_connected", status : "success", url: TRADER_NET_URL}) 
, (err) => {
	console.log({oper: "tn_connected", status : "error", err: err, url: TRADER_NET_URL})
	process.exit(1);
})

var pubOpts = {uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE};
var pub = new rabbit.RabbitPub(pubOpts); 
pub.connect();
pub.connectStream.subscribe(() =>
 console.log({oper: "rabbitmq_connected", status : "success", opts: pubOpts}) 
, (err) => {
	console.log({oper: "rabbitmq_connected", status : "error", err: err, opts: pubOpts});
	process.exit(1);
});

 
 