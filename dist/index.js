/// <reference path="../typings/tsd.d.ts" />
var rabbit = require("da-rabbitmq-rx");
var logs = require("da-logs");
var Rx = require("rx");
var tn = require("da-trader-net-rx");
function getEnvVar(name) {
    return process.env[name] ||
        process.env["npm_config_" + name] ||
        process.env["npm_package_config_" + name];
}
var TRADER_NET_URL = getEnvVar("TRADER_NET_URL");
var TRADER_NET_API_KEY = getEnvVar("TRADER_NET_API_KEY");
var TRADER_NET_SEC_KEY = getEnvVar("TRADER_NET_SEC_KEY");
var RABBIT_URI = getEnvVar("RABBIT_URI");
var RABBIT_QUEUE = getEnvVar("RABBIT_QUEUE_QUOTES");
var TRADER_NET_QUOTES_RECONNECT_TIMEOUT = getEnvVar("TRADER_NET_QUOTES_RECONNECT_TIMEOUT");
var logger = new logs.LoggerCompose({ pack: require("../package.json"), tags: ["pipe", "quotes"] }, { console: true });
var traderNet;
function reconnect() {
    traderNet.quotesStream
        .map(function (val) { return val.filter(function (f) { return !!f.latestPrice; }); })
        .filter(function (val) { return val.length != 0; })
        .subscribe(function (val) {
        pub.write(val);
    });
    traderNet.startRecieveQuotes(getEnvVar("TRADER_NET_TICKETS").split(","));
}
console.log(TRADER_NET_QUOTES_RECONNECT_TIMEOUT);
Rx.Observable.timer(0, TRADER_NET_QUOTES_RECONNECT_TIMEOUT ? parseInt(TRADER_NET_QUOTES_RECONNECT_TIMEOUT) : undefined)
    .flatMap(function (_) {
    if (traderNet) {
        traderNet.disconnect();
    }
    traderNet = new tn.TraderNet(TRADER_NET_URL);
    return traderNet.connect({ apiKey: TRADER_NET_API_KEY, securityKey: TRADER_NET_SEC_KEY });
})
    .do(reconnect)
    .subscribe(function () {
    return console.log({ oper: "tn_connected", status: "success", url: TRADER_NET_URL });
}, function (err) {
    console.log({ oper: "tn_connected", status: "error", err: err, url: TRADER_NET_URL });
    process.exit(1);
});
var pubOpts = { uri: RABBIT_URI, socketType: rabbit.SocketType.PUB, queue: RABBIT_QUEUE };
var pub = new rabbit.RabbitPub(pubOpts);
pub.connect();
pub.connectStream.subscribe(function () {
    return console.log({ oper: "rabbitmq_connected", status: "success", opts: pubOpts });
}, function (err) {
    console.log({ oper: "rabbitmq_connected", status: "error", err: err, opts: pubOpts });
    process.exit(1);
});
