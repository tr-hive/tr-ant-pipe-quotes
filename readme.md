# Trader micro service

## Project structure

+ /src - sources, type script  
  + index.ts - entry point of application
    
+ Dockerfile - docker image builder
+ .envs - define you config variables here (ignored in git)
+ typings - typescript definition files    
  
## Prerequisites

+ nodejs
+ npm
+ typescript


## Install

```
git clone https://bitbucket.org/tr-hive/tr-ant-pipe-quotes
npm install
```  
+ To rebuild `tsc` or `npm run-script build`
+ To watch and rebuild `tsc -w`

Rebuild also executed before each `npm start` 

## Start

+ Define enviroment variables, in order of priority `env`, `.npmrc`, `package.json`
+ Test start `npm start` 

## Docker 

+ Build container `docker build -t baio/tr-ant-pipe-quotes .`
+ Start container `docker run baio/tr-ant-pipe-quotes`

Also build `npm run-script docker` or `npm run-script docker-clean`  

    "da-logs": /*"file:../../da-libs/da-logs"*/"latest",
    "da-rabbitmq-rx": /*"file:../../da-libs/da-rabbitmq-rx"*/"latest",
    "da-trader-net-rx": /*"file:../../da-libs/da-trader-net-rx"*/"latest",
